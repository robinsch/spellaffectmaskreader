#include "defines.h"
#include "includes.h"
#include "DBCStores.h"

using namespace std;

inline string maskToHex(uint64_t mask)
{
    std::stringstream ss;
    ss << "0x" << setfill('0') << setw(sizeof(int) * 2) << hex << uppercase << mask;
    return ss.str();
}

inline bool stringCompare(const string &stringOne, const string &stringTwo)
{
    return (stringOne == stringTwo);
}
