#ifndef LOG_H
#define LOG_H

class Log
{
    public:
        Log();
        ~Log();

        void Initialize();
        void outResult(const char* str);

    private:
        FILE* _logFile;
        std::string _logFileName;
        std::string _logsDir;
};

#endif
